FROM python:alpine

RUN apk add curl
RUN pip install flask==2.0.3

ADD /app.py /app/app.py
WORKDIR /app

HEALTHCHECK CMD curl --fail http://localhost:5000/health || exit 1

CMD python app.py
